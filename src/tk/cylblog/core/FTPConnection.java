/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.cylblog.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/**
 *
 * @author CYL
 */
public class FTPConnection {

    String host;
    int port;
    String username;
    String password;
    FTPClient ftp;
    Watcher watcher = new Watcher();  //临时使用

    public FTPConnection(String host, String port, String username, String password) {
        this.host = host;
        this.port = Integer.parseInt(port);
        this.username = username;
        this.password = password;
        ftp = new FTPClient();
    }

    public void connect() throws IOException {
        try {
            ftp.connect(host, port);
            ftp.setControlEncoding("UTF-8");

            //FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_NT);  

            //conf.setServerLanguageCode("zh");  
        } catch (UnknownHostException ex) {
            watcher.update("UNKNOWN HOST! PLEASE CHECK YOUR HOST ADDRESS!", InfoType.ERROR);
            ftp.disconnect();
        } catch (ConnectException ex) {
            watcher.update("CONNECTION REFUSED! PLEASE CHECK YOUR HOST ADDRESS, PORT, AND YOUR NETWORK!", InfoType.ERROR);
            ftp.disconnect();
        } catch (SocketException ex) {
            Logger.getLogger(FTPConnection.class.getName()).log(Level.SEVERE, null, ex);
            ftp.disconnect();
        } catch (IOException ex) {
            Logger.getLogger(FTPConnection.class.getName()).log(Level.SEVERE, null, ex);
            ftp.disconnect();
        }
        int reply = ftp.getReplyCode();
        watcher.update("[Code:" + reply + "]Successful connection!", InfoType.OK);
        login();
    }

    private void login() throws IOException {
        ftp.login(username, password);
        int reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            watcher.update("[Code:" + reply + "]INCORRECT USERNAME OR PASSWORD!", InfoType.ERROR);
        } else {
            watcher.update("[Code:" + reply + "]Successful login!", InfoType.OK);
        }

        listFile("/");
    }

    public void listFile(String pathname) throws IOException {
        FTPFile[] ftpfile;
        if (pathname == null) {
            ftp.changeWorkingDirectory("/");
            ftpfile = ftp.listFiles();
        } else {
            ftpfile = ftp.listFiles(pathname);
        }
        int reply = ftp.getReplyCode();
        watcher.update("[Code:" + reply + "]Get the file list", InfoType.INFO);
        for (FTPFile file : ftpfile) {

            if (file.isDirectory()) {
                System.out.println("[Directory]" + file.getName());
            } else {
                System.out.println("[File]" + file.getName());
            }
        }

    }

    public void download(String filename, String localPath) throws IOException {
        //InputStream in=ftp.retrieveFileStream(filename);
        //System.out.println(in);
        FileOutputStream fos = null;
        File file = new File(localPath + "/" + filename);
        try {
            fos = new FileOutputStream(file);
        } catch (Exception ex) {
            int reply = ftp.getReplyCode();
            watcher.update("[Code:" + reply + "]文件下载失败 " + ex.getMessage(), InfoType.ERROR);
            return;
        }
        try {
            ftp.retrieveFile(filename, fos);
            int reply = ftp.getReplyCode();
            watcher.update("[Code:" + reply + "]文件下载成功", InfoType.OK);
        } catch (Exception ex) {
            int reply = ftp.getReplyCode();
            watcher.update("[Code:" + reply + "]文件下载失败 " + ex.getMessage(), InfoType.ERROR);
        } finally {
            fos.close();
        }
    }

    public void upload(String filename, String localPath) {
        File file = new File(localPath + "/" + filename);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            int reply = ftp.getReplyCode();
            watcher.update("[Code:" + reply + "]文件上传失败 " + ex.getMessage(), InfoType.ERROR);
            return;
        }
        try {
            ftp.storeFile(filename, fis);
            int reply = ftp.getReplyCode();
            watcher.update("[Code:" + reply + "]文件上传成功", InfoType.OK);
        } catch (IOException ex) {
            int reply = ftp.getReplyCode();
            watcher.update("[Code:" + reply + "]文件上传失败 " + ex.getMessage(), InfoType.ERROR);
        }
    }

    public static void main(String[] args) throws Exception {
        //FTPConnection ftp = new FTPConnection("localhost", "21", "test", "test");
        //ftp.connect();
        //ftp.upload("iTools0111.zip", "c:/QQdownload");
    }
}
